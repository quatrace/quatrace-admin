import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import store from '@/vuex/store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      meta: {
        requiresAuth: true
      },
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/settings',
          name: 'settings',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Settings.vue')
        },
      ]
    },
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      meta: {
        requiresAuth: false
      },
      children: [
        {
          path: '/login',
          name: 'login',
          meta: {
            requiresAuth: false
          },
          component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue'),
          beforeEnter(routeTo, routeFrom, next) {
            if (store.state.account.isLogged) {
              next({ name: 'dashboard' });
            } else {
              next();
            }
          },
        }
      ]
    }
  ]
});

router.beforeEach((routeTo, routeFrom, next) => {
  if (routeTo.matched.some(record => record.meta.requiresAuth !== false)) {
    if (store.state.account.isLogged) {
      next();
      return;
    } else {
      store.dispatch('redirect/setRedirect', routeTo.fullPath);
      next({ name: 'login' });
    }
  } else {
    next();
  }
});

export default router;
