/*!

=========================================================
* Quatrace - v1.0.0
=========================================================

*/
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./vuex/store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "./assets/scss/main.scss";
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

Vue.config.productionTip = false;

Vue.use(Vuetify, {
  theme: {
    primary: "#1c3050",
    accent: "#0288d1",
    backgroundColor: "#c6deef"
  },
});

 
Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: 'AIzaSyAbIrn27Ge8bDXUOyUx67CmsCRtxziSEnc', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
  language: 'bg-BG', // Optional
  version: '3.38'
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
