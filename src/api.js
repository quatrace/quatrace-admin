import axios from 'axios';
import store from '@/vuex/store';

const api = axios.create({
  baseURL: 'https://quatrace.com/api/',
  withCredentials: false, // This is the default
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
});

api.interceptors.request.use(
  function(config) {
    if (
      store.state.account.isLogged
    ) {
      config.headers.Authorization = `Bearer ${
        store.state.account.auth.access_token
      }`;
    }
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default api;
