import api from '@/api.js';

export default {
  getSettings: () => api.get('/admin/settings'),
};
