import api from '@/api.js';

export default {
  login: (email, password) => api.post('/auth/login', { email, password }),

  logout: (params) => api.get('/logout', { params }),

  account: (params) => api.get('/me', { params })
};
