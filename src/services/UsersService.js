import api from '@/api.js';

export default {
  users: (params) => api.get('/admin/users', { params }),
  createUser: (userObject) => api.post('/admin/users', userObject),
  verifyUser: (bodyParams) => api.post('/admin/verifications/new', bodyParams)
};
