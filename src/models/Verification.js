import moment from 'moment';


export default class Verification {
    constructor(data) {
        this.lat = data.lat ? data.lat : 'N/A'
        this.lng = data.lng ? data.lng : 'N/A',
        this.reason = data.reason ? data.reason : 'N/A'
        this.status = this.parseStatus(data.status)
        this.distance = data.distance ? data.distance : 'N/A'
        this.isImageIdentical = data.is_identical ? data.is_identical : 'N/A'
        this.confidence = data.confidence ? data.confidence : 'N/A'
        this.sendAt = moment.unix(data.pn_send_at).format("DD/MM/YYYY HH:mm");
        this.confirmed = moment.unix(data.updated_at).format("DD/MM/YYYY HH:mm");
    }

    parseStatus(status) {
        if(this.reason.toLowerCase() === 'expired') {
            return 'Expired'
        }
        return `${status.slice(0, 1).toUpperCase()}${status.slice(1, status.length).toLowerCase()}`
    }
}
