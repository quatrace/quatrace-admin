import moment from "moment";
import Verification from "./Verification";

export default class User {
  constructor(data) {
    this.id = data.id
    this.name = data.name;
    this.email = data.email;
    this.phone = data.quarantine.phone;
    this.quarantineStartedAt = moment
      .unix(data.quarantine.created_at)
      .format("DD/MM/YYYY");
    this.quarantineEndsAt = moment
      .unix(data.quarantine.expires_at)
      .format("DD/MM/YYYY");
    this.verifications = data.verifications.map(
      (item) => new Verification(item)
    );
    this.fcmKey = data.quarantine.fcm_key == null ? false : true
  }
}
