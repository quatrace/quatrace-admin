/* eslint-disable */
import AccountService from '@/services/AccountService.js';
import router from '@/router';

export const namespaced = true;

export const state = {
  loading: false,
  isLogged: false,
  error: null,
  auth: null,
  avatar: 'https://en.gravatar.com/userimage/5301100/e60a9545ca47a1a9b2a06edc64554a54.png?size=300',
  name: '',
  email: ''
};

export const actions = {
  async login({ commit, dispatch }, { email, password }) {
    commit('LOGIN_REQUEST');

    try {
      const response = await AccountService.login(email, password);
      const auth = response.data;
      commit('LOGIN_SUCCESS', auth);
      localStorage.setItem('auth', JSON.stringify(auth));
      await dispatch('getAccount');
      await this.dispatch('redirect/redirectAfterLogin');
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'success',
        message: 'Logged in!',
      })
      return auth;
    } catch (error) {
      console.log(error.message);
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'error',
        message: 'Login failed!',
      })
      commit('LOGIN_FAILURE', error);
    }
  },

  async setSavedAuth({ commit, dispatch }, auth) {
    commit('LOGIN_SUCCESS', auth);
    await dispatch('getAccount');
  },

  async getAccount({ commit }) {
    try {
      const response = await AccountService.account();
      const account = response.data;
      commit('SET_ACCOUNT', account);
      return account;
    } catch (error) {
      console.log('Couldn`t fetch account details', error);
    }
  },

  logout({ dispatch }) {
    AccountService.logout()
      .then(() => {
        dispatch('removeAuth');
      })
      .catch(error => {
        console.log(error.message);
      });
  },

  removeAuth({ commit }) {
    localStorage.removeItem('auth');
    commit('LOGOUT');
    router.push({ name: 'login' });
  }
};

export const mutations = {
  LOGIN_REQUEST() {
    state.loading = true;
  },

  LOGIN_SUCCESS(state, auth) {
    state.loading = false;
    state.error = null;
    state.isLogged = true;
    state.auth = auth;
  },

  LOGIN_FAILURE(state, error) {
    state.loading = false;
    state.error = error;
    state.isLogged = false;
    state.auth = null;
  },

  SET_ACCOUNT(state, { name, email }) {
    state.name = name;
    state.email = email;
  },

  LOGOUT(state) {
    state.loading = false;
    state.isLogged = false;
    state.auth = null;
  }
};
