/* eslint-disable */
import UsersService from "@/services/UsersService.js";
import User from "@/models/User";

export const namespaced = true;

export const state = {
  list: [],
  verificationRequests: [],
};

export const actions = {
  async fetch({ commit }) {
    try {
      const response = await UsersService.users();
      commit("SET_USERS", response.data);
    } catch (error) {
      console.log(error.message);
    }
  },

  async requestVerification({ commit }, user_id) {
    commit("ADD_TO_SENDING_LIST", user_id);

    try {
      await UsersService.verifyUser({ user_id });
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'success',
        message: 'Verification request sended!',
      })
    } catch (error) {
      console.log(error.message);
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'error',
        message: 'Something went wrong!',
      })
    } finally {
      commit("REMOVE_FROM_SENDING_LIST", user_id);
    }
  },
  async registerUser({ dispatch }, userObject) {
    try {
      await UsersService.createUser(userObject);
      await dispatch("fetch");
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'success',
        message: 'User registered successfully!',
      })
    } catch (error) {
      console.log(error);
      this.dispatch('common/setSnackbar', {
        show: true,
        type: 'error',
        message: 'Something went wrong!',
      })
    }
  },
};

export const mutations = {
  SET_USERS(state, payload) {
    state.list = payload;
  },

  ADD_TO_SENDING_LIST(state, payload) {
    state.verificationRequests.push(payload);
  },

  REMOVE_FROM_SENDING_LIST(state, payload) {
    state.verificationRequests = state.verificationRequests.filter(
      (id) => id !== payload
    );
  },
};

export const getters = {
  getFormattedUsers(state) {
    return state.list.map((user) => new User(user));
  },
  getRawUsers(state) {
    return state.list;
  },
};
