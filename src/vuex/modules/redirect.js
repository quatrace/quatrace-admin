import router from '@/router';

export const namespaced = true;

export const state = {
  redirectTo: null
};

export const actions = {
  setRedirect({ commit }, path) {
    commit('SET_REDIRECT', path);
  },

  removeRedirect({ commit }) {
    commit('REMOVE_REDIRECT');
  },

  redirectAfterLogin({ state, dispatch }) {
    if (state.redirectTo) {
      router.push(state.redirectTo);
    } else {
      router.push({ name: 'dashboard' });
    }

    dispatch('removeRedirect');
  }
};

export const mutations = {
  SET_REDIRECT(state, path) {
    state.redirectTo = path;
  },

  REMOVE_REDIRECT(state) {
    state.redirectTo = null;
  }
};
