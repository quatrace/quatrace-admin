/* eslint-disable */
import SettingsService from "@/services/SettingsService.js";
export const namespaced = true;

export const state = {
  settings: {},
};

export const actions = {
  async setSettings({ commit }, settingsObject) {
    try {
      await SettingsService.setSettings(settingsObject);
      commit("SET_SETTINGS", settingsObject);
      this.dispatch("common/setSnackbar", {
        show: true,
        type: "success",
        message: "Settings successfully update!",
      });
    } catch (error) {
      console.log(error);
      this.dispatch("common/setSnackbar", {
        show: true,
        type: "error",
        message: "Something went wrong!",
      });
    }
  },
  async fetchSettings({ commit }) {
    try {
      const { data } = await SettingsService.getSettings();
      commit("SET_SETTINGS", data);
    } catch (error) {
      console.log(error);
      this.dispatch("common/setSnackbar", {
        show: true,
        type: "error",
        message: "Something went wrong!",
      });
    }
  },
};

export const mutations = {
  SET_SETTINGS(state, settingsObject) {
    state.settings = Object.assign({}, settingsObject);
  },
};

export const getters = {
  getSettings(state) {
    return Object.assign({}, state.settings);
  },
};
