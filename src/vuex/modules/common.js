export const namespaced = true

export const state = {
  snackbar: {
    show: false,
    type: "",
    message: "",
    timeout: 3000,
  },
};
export const actions = {
  setSnackbar({ commit }, snackbarObject) {
    commit("SET_SNACKBAR", snackbarObject);
  },
};
export const mutations = {
  SET_SNACKBAR(state, snackbarObject) {
    const clonedSnackbar = Object.assign({}, state.snackbar);
    Object.assign(clonedSnackbar, snackbarObject);
    state.snackbar = clonedSnackbar;
  },
};
export const getters = {
  getSnackbar(state) {
    return state.snackbar;
  },
};


