import Vue from 'vue';
import Vuex from 'vuex';
import * as account from '@/vuex/modules/account.js';
import * as redirect from '@/vuex/modules/redirect.js';
import * as users from '@/vuex/modules/users.js';
import * as common from '@/vuex/modules/common.js';
import * as settings from '@/vuex/modules/settings.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    account,
    redirect,
    users,
    common,
    settings,
  },
  state: {}
});

export default store;

const auth = JSON.parse(localStorage.getItem('auth'));

if (auth) {
  store.dispatch('account/setSavedAuth', auth);
}
